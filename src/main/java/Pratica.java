import utfpr.ct.dainf.pratica.*;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica {

    public static void main(String[] args) {
        Ponto2D p1 = new PontoXZ(-3,2);
        Ponto2D p2 = new PontoXZ(-3,6);
        Ponto2D p3 = new PontoXZ(0,2);
        
        PoligonalFechada pf = new PoligonalFechada();
        pf.set(0, p1);
        pf.set(1, p2);
        pf.set(2, p3);
        
        double res = pf.getComprimento();
        System.out.println("Comprimento da poligonal = " + res);
    }    
}
