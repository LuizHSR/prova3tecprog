/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.pratica;

/**
 *
 * @author a1795406
 */
public class PoligonalFechada  extends Poligonal{
    double auxRes = this.getComprimento();
    
    @Override
    public double getComprimento()
    {
        double res;
        Ponto2D aux1;
        Ponto2D aux2;
        
        aux1 = this.get(0);
        aux2 = this.get(this.getN() - 1);
        
        res = aux1.dist(aux2);
        res = res + auxRes;
        return res;
    }
}
