package utfpr.ct.dainf.pratica;

import java.util.ArrayList;
import java.util.List;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 * @param <T> Tipo do ponto
 */
public class Poligonal<T extends Ponto2D> {
    private final List<T> vertices = new ArrayList<>();
    
    public int getN()
    {
        int aux;
        
        aux = vertices.size();
        return aux;
    }
    public T get(int i)
    {
        if(i > vertices.size() || i < 0)
        {
            throw new RuntimeException("get(" + i + "):indice invalido");
        }
        T aux;
        
        aux = vertices.get(i);
        
        return aux;
    }
    public void set(int i, T p)
    {
        if(i > vertices.size() || i < 0)
        {
            throw new RuntimeException("set(" + i + "):indice invalido");
        }
        if(i == vertices.size())
        {
            vertices.add(p);
        }
        else
        {
            vertices.add(i, p);
        }
    }
    public double getComprimento()
    {
        T auxP1;
        T auxP2;
        double res = 0;
        
        for(int i = 1; i < vertices.size(); i++)
        {
            auxP1 = vertices.get(i-1);
            auxP2 = vertices.get(i);
            res = res + auxP1.dist(auxP2);
        }
        return res;
    }
}
